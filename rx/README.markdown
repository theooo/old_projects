# rx
### (previously rf)
<hr>

[{video} rx in action.](.assets/rf.mp4)

Open files and surf through folders with ease.  
Powered by [fd](https://github.com/sharkdp/fd) and [fzf](https://github.com/junegunn/fzf).

### Installation

```
git clone https://gitlab.com/chanceboudreaux/rx.git
cd rx
cp rx $HOME/.local/bin/
source $HOME/.local/bin/rx
```

### Usage

```
  rx: open files, cd into directories and stuff
  uses fd and fzf.
  usage: rx [options]

  OPTIONS

  => no arguments 
	 open files via $opener. (xdg-open is set as default)
  => -l 
	 open files without closing fzf.
  => -d 
	 open directories.
  => -h / --help 
	 display help text.
  => -z 
	 search for files and open the directory in which they are located.

```
