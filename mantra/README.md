# mantra

[{video} mantra in action.](.assets/mantra.mp4)

<div style="background-color: #082437; padding: 0.5rem;">
Superseded by <a href="https://codeberg.org/theooo/mantra.py/">mantra.py</a>
</div>

View online manual pages (from https://man.archlinux.org) from the terminal.  
Written in bash.

## Note

`mantra` is not meant to be an alternative to man. `mantra` is a tool to view manpages of a command without downloading and installing the whole package.

## Dependencies

- [curl](https://curl.se/)
- [fzf](https://github.com/junegunn/fzf)
- [pup](https://github.com/EricChiang/pup)

## Usage

```
Usage: mantra [OPTION]...

  -k/--keyword <query>  keyword
  -d/--download         download the manual page
  -V/--version          print version
  -h/--help             display this help and exit
```

## Installation

```
git clone https://gitlab.com/chanceboudreaux/mantra.git
cd mantra/
sudo make install
```

or

```
sudo curl -sL "https://gitlab.com/chanceboudreaux/mantra/-/raw/main/mantra" -o /usr/local/bin/mantra 
sudo chmod +x /usr/local/bin/mantra
```
