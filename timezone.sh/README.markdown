## timezone.sh

Superseded by [tzfzf](https://codeberg.org/theooo/tzfzf)

![image](timezone.jpg)

A dead-simple bash script to display a list of timezones and their respective times. Got this idea when my friend moved to Canada.

#### Requirements

Just the ```date``` command and fzf.
